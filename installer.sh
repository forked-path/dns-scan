#!/bin/bash
APP_NAME="dns-lookup"
CGO_ENABLED=0 go build -o ${APP_NAME} -ldflags="-s -w" .
sudo cp ${APP_NAME} /usr/bin/